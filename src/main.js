
import { createApp } from 'vue'
// import { createPinia } from 'pinia'

import App from './App.vue'
// import router from "@/router/index.js"
// import JwPagination from 'jw-vue-pagination'


// import 'bootstrap/dist/js/bootstrap.bundle'
import 'bootstrap/dist/js/bootstrap.bundle'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap/dist/css/bootstrap.min.css'
// import 'bootstrap'

import "./main.scss"
// import mitt from "mitt"

// const emitter = mitt()
const app = createApp(App)
// app.use(createPinia())
// app.config.globalProperties.emitter = emitter
app.mount('#app')

